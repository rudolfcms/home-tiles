<?php

use Rudolf\Component\Helpers\Pagination\Calc as Pagination;
use Rudolf\Component\Helpers\Pagination\Loop;
use Rudolf\Component\Images\Image;
use Rudolf\Component\Hooks;

include 'Model.php';

Hooks\Filter::add('head_stylesheets', function ($stylesheets) {
    $stylesheets[] = PLUGINS.'/home-tiles/style.css';

    return $stylesheets;
});

Hooks\Action::add('home_page_slider', function () {
    $articles = (new Model())->getItems(5);

    if (false === $articles) {
        return;
    }

    $this->loop = new Loop($articles, new Pagination(5),
        'Rudolf\\Modules\\Articles\\One\\Article'
    );

    $html[] = '<div class="home-tiles" id="tiles">';

    $current = 1;
    while ($this->loop->haveItems()) {
        $item = $this->loop->item();
        switch ($current++) {
            case 1:
                $class = 'tile-big tile-big-60';
                $width = 650;
                $height = 270;
                break;
            case 2:
                $class = 'tile-big tile-big-40';
                $width = 650;
                $height = 270;
                break;
            case 3:
            case 4:
            case 5:
                $class = 'tile-small';
                $width = 650;
                $height = 200;
                break;

            default:
                # code...
                break;
        }

        $html[] = sprintf('
            <a href="%1$s">
                <div class="tile %2$s" style="background:url(%3$s);">
                    <h3>%4$s</h3>
                </div>
            </a>',
            $item->url(),
            $class,
            Image::resize($item->thumb(), $width, $height),
            $item->title()
        );
    }

    $html[] = '</div>';
    echo implode("\r\n", $html);
});
